import 'intersection-observer';
import { Component, OnInit, Input, SimpleChanges, SimpleChange } from '@angular/core';
import { PumpwoodAuthService } from './../pumpwood-auth.service'
import { PumpwoodRestService } from './../pumpwood-rest.service'
import { IListArgs, IPumpWoodObj } from './../interfaces.interface'
import { Router, ActivatedRoute, Params } from '@angular/router'
import { Observable } from 'rxjs/Observable';
import * as _ from "lodash";

export interface IModalDict {
  [key: string]: Component;
}

export interface IOnChange{
  'key': string;
  'value':any;
};

/**
/*Default component to retrieve list data from pumpwood
*@param model_class - Model class that will be listed on the view
*@param start_query_dict - Query parameters that will be paginated
*@param pk_column - Name of the column to be considered as pk.
**/
@Component({
  selector: 'app-pumpwood-retrive',
  template: '<ng-content></ng-content>'
})
export class PumpwoodRetrieveComponent {
  @Input() model_class:string;
  @Input() pk:number;
  @Input() get_fill_options:boolean = false;
  @Input() retrieve_simple:boolean = false;

  working:boolean = false;
  fill_working:boolean = false;
  object_data:IPumpWoodObj | undefined;
  temp_object_data:IPumpWoodObj | undefined;
  object_fill_options:object;

  constructor(private rest_service:PumpwoodRestService
    , private router: Router) {
  }

  getData():Observable<IPumpWoodObj>{
    let observable:Observable<IPumpWoodObj>;

    this.working = true;
    if (this.retrieve_simple){
      observable = this.rest_service.list_one(this.model_class, this.pk);
    }else{
      observable = this.rest_service.retrieve(this.model_class, this.pk);
    }

    observable.subscribe(
      data => {
        this.object_data = data;
        this.temp_object_data = data;
        this.getFillOptions();
        this.working = false;
      }
    )
    return observable
  }

  getFillOptions():Observable<any> | null{
    let observable:any = null;

    if (this.get_fill_options || false){
      this.fill_working = true;
      observable = this.rest_service.fill_options(this.temp_object_data);
      observable.subscribe(
        data => {
          this.object_fill_options = data;
          this.fill_working = false;
      });
    }
    return observable
  }

  saveObject(event_data:any){
    let observable:Observable<IPumpWoodObj>;

    observable = this.rest_service.save(this.temp_object_data)
    observable.subscribe(
      data =>{
        this.object_data = _.cloneDeep(data);
        this.temp_object_data = _.cloneDeep(data);
      }
    );
    return observable
  }

  changeObject(change_value:IOnChange){
    this.temp_object_data[change_value['key']] = change_value['value'];
    this.temp_object_data = _.cloneDeep(this.temp_object_data);
  }

  undoChanges(event_data:any, object_key:string=null ){
    if (object_key === null){
      this.temp_object_data = _.cloneDeep(this.object_data);
    }else{
      this.temp_object_data[object_key] = _.cloneDeep(this.object_data[object_key]);
      this.temp_object_data = _.cloneDeep(this.temp_object_data);
    }
  }

  deleteObject(event_data:any){
    this.rest_service.delete(this.object_data).subscribe(
      data => {
      }
    );
  }

  ngOnChanges(changes:SimpleChanges){
    let pk__change:SimpleChange | undefined;
    let temp_object_data__change:SimpleChange | undefined;
    let model_class__change:SimpleChange | undefined;

    pk__change = changes.pk;
    temp_object_data__change = changes.temp_object_data;
    model_class__change = changes.model_class;

    if (pk__change != undefined){
      if(pk__change.currentValue != undefined){
        this.getData();
      }
    }

    if (model_class__change != undefined){
      this.temp_object_data = {
        "model_class": model_class__change.currentValue,
      };

      this.temp_object_data = _.cloneDeep(this.temp_object_data);
    }

    if (temp_object_data__change != undefined){
      this.getFillOptions();
    }
  }

  redirect(){
    let model_class:string;
    let url:string;

    model_class = this.object_data.model_class;
    url = '/gui/endpoint/' + model_class.toLowerCase() + '/retrieve/' + this.object_data.pk;
    this.router.navigateByUrl(url);
  }
}
