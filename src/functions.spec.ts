import { mergeOrderedObjectLists } from './functions'
import { expect } from 'chai';
import 'mocha';

let list_1_asc:object[] = [
  {'id': 1, 'name': 'a'},
  {'id': 2, 'name': 'b'},
  {'id': 4, 'name': 'f'},
  {'id': 5, 'name': 'g'},
  {'id': 10, 'name': 'h'},
]

let list_2_asc:object[] = [
  {'id': 2, 'name': 'c'},
  {'id': 6, 'name': 'd'},
  {'id': 7, 'name': 'e'},
  {'id': 8, 'name': 'i'},
  {'id': 9, 'name': 'j'},
]

let list_1_desc:object[] = [
  {'id': 10, 'name': 'h'},
  {'id': 5, 'name': 'g'},
  {'id': 4, 'name': 'f'},
  {'id': 2, 'name': 'b'},
  {'id': 1, 'name': 'a'},
]

let list_2_desc:object[] = [
  {'id': 9, 'name': 'j'},
  {'id': 8, 'name': 'i'},
  {'id': 7, 'name': 'e'},
  {'id': 6, 'name': 'd'},
  {'id': 2, 'name': 'c'},
]

let expected_list_id = [
  {'id': 1, 'name': 'a'},
  {'id': 2, 'name': 'b'},
  {'id': 2, 'name': 'c'},
  {'id': 4, 'name': 'f'},
  {'id': 5, 'name': 'g'},
  {'id': 6, 'name': 'd'},
  {'id': 7, 'name': 'e'},
  {'id': 8, 'name': 'i'},
  {'id': 9, 'name': 'j'},
  {'id': 10, 'name': 'h'},
]

let expected_list_name = [
  {'id': 1, 'name': 'a'},
  {'id': 2, 'name': 'b'},
  {'id': 2, 'name': 'c'},
  {'id': 6, 'name': 'd'},
  {'id': 7, 'name': 'e'},
  {'id': 4, 'name': 'f'},
  {'id': 5, 'name': 'g'},
  {'id': 10, 'name': 'h'},
  {'id': 8, 'name': 'i'},
  {'id': 9, 'name': 'j'},
]

describe('Order ascending number', () => {
  it('It should return ordered merged vector', () => {
    const result = mergeOrderedObjectLists(list_1_asc, list_2_asc, ['id'])
    expect(result).deep.equal(expected_list_id);
  });
});

describe('Order descending number', () => {
  it('It should return ordered merged vector', () => {
    const result = mergeOrderedObjectLists(list_2_desc, list_1_desc, ['-id'])
    expect(result).deep.equal(expected_list_id.reverse());
  });
});

describe('Order ascending string', () => {
  it('It should return ordered merged vector', () => {
    const result = mergeOrderedObjectLists(list_1_asc, list_2_asc, ['name'])
    expect(result).deep.equal(expected_list_name);
  });
});

describe('Order descending string', () => {
  it('It should return ordered merged vector', () => {
    const result = mergeOrderedObjectLists(list_2_desc, list_1_desc, ['-name'])
    expect(result).deep.equal(expected_list_name.reverse());
  });
});
