import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ClearListComponentService {
  private onBroadcastClearListComponent = new Subject<string>();
  onBroadcastClearListComponent$ = this.onBroadcastClearListComponent.asObservable();
  constructor() { }

  clearListComponent(list_name:string){
    this.onBroadcastClearListComponent.next(list_name);
  }
}
