import { Observable } from 'rxjs/Observable';

export function fromEvent(target, eventName) {
  return new Observable((observer) => {
    const handler = (e) => observer.next(e);

    // Add the event handler to the target
    target.addEventListener(eventName, handler);

    return () => {
      // Detach the event handler from the target
      target.removeEventListener(eventName, handler);
    };
  });
}

export function mergeOrderedObjectLists(list_1:object[], list_2:object[],
                                        order_atts:string[]){
  let return_list:object[] = []
  let i_list_1:number = 0;
  let i_list_2:number = 0;
  let n_list_1:number = list_1.length
  let n_list_2:number = list_2.length

  if (0 < order_atts.length){
    while (i_list_1 < n_list_1 && i_list_2 < n_list_2){
      let obj_list_1:object = list_1[i_list_1];
      let obj_list_2:object = list_2[i_list_2];
      let i_att:number = 0;

      while (true){
        let att = order_atts[i_att];
        let ascending = true;
        if (att.charAt(0) == '-'){
          ascending = false;
          att = att.slice( 1 );
        }

        if (obj_list_1[att] < obj_list_2[att]){
          if (ascending){
            return_list.push(obj_list_1);
            i_list_1 = i_list_1 + 1;
            break;
          }else{
            return_list.push(obj_list_2);
            i_list_2 = i_list_2 + 1;
            break;
          }
        }

        if (obj_list_2[att] < obj_list_1[att]){
          if (ascending){
            return_list.push(obj_list_2);
            i_list_2 = i_list_2 + 1;
            break;
          }else{
            return_list.push(obj_list_1);
            i_list_1 = i_list_1 + 1;
            break;
          }
        }

        if (obj_list_1[att] == obj_list_2[att]){
          if (i_att == (order_atts.length - 1)){
            return_list.push(obj_list_1);
            i_list_1 = i_list_1 + 1;
            break;
          }
        }
        i_att = i_att + 1;
      }
    }
  }

  while (i_list_1 < n_list_1){
    let obj_list_1:object = list_1[i_list_1];
    return_list.push(list_1[i_list_1]);
    i_list_1 = i_list_1 + 1;
  }

  while (i_list_2 < n_list_2){
    return_list.push(list_2[i_list_2]);
    i_list_2 = i_list_2 + 1;
  }

  return return_list
}
