//PumpWood object
export interface IPumpWoodObj {
    pk?: number;
    model_class: string;
    [key:string]: any;
};

//Args for list call
export interface IListArgs {
    filter_dict?: object;
    exclude_dict?: object;
    order_by?: string[];
};


//Returns de result of an action
export interface IActionResult{
  action:string;
  object:IPumpWoodObj | null;
  parameters: {
    [key:string]:any;
  }
  result:any;
}

//Description of choice fields in search
export interface ISearchOptions {
  [key:string]: {
    'value': any;
    'description': string;
  }[];
}

export interface IParameterOptions{
  "type": string;
  "required": boolean;
  [key:string]:any;
}

export interface IActionDescription{
  action_name:string;
  info:string;
  parameters:{
    [key:string]: IParameterOptions
  };
}
