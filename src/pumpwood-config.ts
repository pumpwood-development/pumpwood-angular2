
export class RestConfig {
  rest_url:string;
  gui_url:string;

  constructor(private apiUrlRest:string, private apiUrlGui:string){
    this.rest_url = apiUrlRest;
    this.gui_url = apiUrlGui;
  }
}
