import 'rxjs/add/operator/map'
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RestConfig } from './pumpwood-config'


const httpOptions = {
  "default": { 'Content-Type': 'application/json' }
};

/** Interface to login response.*/
interface iLogin {
  token: string;
  csrf_token: string;
}

/** Sevice PumpwoodAuthService responsible PumpWood authentication.*/
@Injectable()
export class PumpwoodAuthService {
  /**
   *Create a PumpwoodAuthService Service
   *@param api_rest_url - the url to PumpWood load balancer
   *@param http - HttpClient
   */
  constructor(private api_rest_url:RestConfig
    , private http: HttpClient){
  }

  private setToken(token:string):void{
    localStorage.setItem('pumpWoodToken', token);
  }

  private getToken():string | null{
    return localStorage.getItem('pumpWoodToken');
  }
  
  buildHeaders():HttpHeaders{
    let header:HttpHeaders;
    let token:string | null;

    header = new HttpHeaders(httpOptions.default);
    token = this.getToken()
    if (token != null){
      return header.append('Authorization',  token);
    }
    return header
  }
  
  login(username: string, password: string):Observable<any> {
      const login_url:string = this.api_rest_url.rest_url + 'registration/login/';
      let header:HttpHeaders;

      header = this.buildHeaders();
      let post_observable = this.http.post(login_url
        , {"username": username, "password": password}
        , {"headers": header, observe: 'response'})
      .map(        
        result => {
          console.log('result:');
          console.log(result);
          this.setToken(<string>(<iLogin>result.body)["token"]);
          return result;
        }
      )
      
      return post_observable
  }

  logout():Observable<any> {
      const  logout_url:string = this.api_rest_url.rest_url + '/registration/logout/';
      let header:HttpHeaders;

      header = this.buildHeaders();
      return this.http.get(logout_url
        , {"headers": header});
  }

  check():Observable<any> {
      const  check_url:string = this.api_rest_url.rest_url + '/registration/check/';
      let header:HttpHeaders;

      header = this.buildHeaders();
      return this.http.get(check_url
        , {"headers": header});
  }

  authenticatedUser():Observable<any> {
      const  authenticated_user_url:string = this.api_rest_url.rest_url + '/registration/retrieveauthenticateduser/';
      let header:HttpHeaders;

      header = this.buildHeaders();
      return this.http.get(authenticated_user_url
        , {"headers": header});
  }
}
