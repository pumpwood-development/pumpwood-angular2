import 'rxjs/add/operator/map'
import { from } from 'rxjs/observable/from';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { PumpwoodAuthService } from './pumpwood-auth.service'
import { RestConfig } from './pumpwood-config'
import { IListArgs, IPumpWoodObj, IActionResult, ISearchOptions,
        IActionDescription } from "./interfaces.interface"


/**
/*Default service to handle communication with PumpWood backend
/*It appends the PumpWood url to the end-points and the authentication
/*token.
**/
@Injectable()
export class PumpwoodRestService {
    /**
     *Create a PumpwoodAuthService Service
     *@param api_rest_url - RestConfig object, contains PumpWood endpoint
     *@param http - HttpClient
     *@param http - PumpwoodAuthService, handle authentication and creates
     */
    constructor(private api_rest_url:RestConfig
      , private http: HttpClient, private auth_api: PumpwoodAuthService){
    }

    /**
     *Make a get request to a PumpWood backend
     *@param url - End-point to make the rest.
     */
    request_get(url:string):Observable<any>{
        let final_url:string;
        let header:HttpHeaders;

        final_url = this.api_rest_url.rest_url + url
        header = this.auth_api.buildHeaders()
        return this.http.get(final_url
          , {"headers": header}
        );
    }

    /**
     *Make a post request to a PumpWood backend
     *@param url - End-point to make the rest.
     *@param data - End-point to make the rest.
     */
    request_post(url:string, data:object):Observable<any>{
        let final_url:string;
        let header:HttpHeaders;

        final_url = this.api_rest_url.rest_url + url
        header = this.auth_api.buildHeaders();
        return this.http.post(final_url
          , data
          , {"headers": header})
    }

    /**
     *Make a delete request to a PumpWood backend
     *@param url - End-point to make the rest.
     */
    request_delete(url:string):Observable<any>{
        let final_url:string;
        let header:HttpHeaders;

        final_url = this.api_rest_url.rest_url + url
        header = this.auth_api.buildHeaders()
        return this.http.delete(final_url
          , {"headers": header}
        );
    }

    /**
     *Retrive an object using an exposed model at PumpWood
     *@param model_class - Name of the model that will be retrieved. Ex.: DescriptionAttribute, ModelQueue.
     *@param pk - pk number of the object.
    **/
    retrieve(model_class:string, pk:number):Observable<IPumpWoodObj>{
        let retrieve_url:string;

        retrieve_url = model_class.toLowerCase() + "/retrieve/" + pk.toString() + "/";
        return this.request_get(retrieve_url)
    }

    /**
     *Retrive an object using list serializer for an exposed model at PumpWood
     *@param model_class - Name of the model that will be retrieved. Ex.: DescriptionAttribute, ModelQueue.
     *@param pk - pk number of the object.
    **/
    list_one(model_class:string, pk:number):Observable<IPumpWoodObj>{
        let retrieve_url:string;

        retrieve_url = model_class.toLowerCase() + "/list-one/" + pk.toString() + "/";
        return this.request_get(retrieve_url)
    }

    /**
     *List objects acording to query parameters using an exposed model at PumpWood. Results will be paginated.
     *@param model_class - Name of the model that will be retrieved. Ex.: DescriptionAttribute, ModelQueue.
     *@param filter_dict - Filter parameters. Used at Where SQL clasuses
     *@param exclude_dict - Filter parameters. Used at where SQL clasuses with not
     *@param order_by - Order the results by the columns. Using '-' at the start of the column name will make ordering descendent
    **/
    list(model_class:string, filter_dict:object | null, exclude_dict:object | null
      , order_by:string[] | null):Observable<IPumpWoodObj[]>{
        let list_url:string;
        let list_post_data:IListArgs;

        list_url = model_class.toLowerCase() + "/list/"
        list_post_data = {};
        if (filter_dict != null) {list_post_data.filter_dict = filter_dict;}
        if (exclude_dict != null) {list_post_data.exclude_dict = exclude_dict;}
        if (order_by != null) {list_post_data.order_by = order_by};
        return  this.request_post(list_url, list_post_data)
    }

    /**
     *List objects acording to query parameters using an exposed model at PumpWood. THIS END-POINT RETURNS ALL DATA... CAREFULL!
     *@param model_class - Name of the model that will be retrieved. Ex.: DescriptionAttribute, ModelQueue.
     *@param filter_dict - Filter parameters. Used at Where SQL clasuses
     *@param exclude_dict - Filter parameters. Used at where SQL clasuses with not
     *@param order_by - Order the results by the columns. Using '-' at the start of the column name will make ordering descendent
    **/
    list_without_pag(model_class:string, filter_dict:object, exclude_dict:object
      , order_by:string[]):Observable<IPumpWoodObj[]>{
        let list_without_pag_url:string;
        let list_post_data:IListArgs;

        list_without_pag_url = model_class.toLowerCase() + "/list-without-pag/"
        list_post_data = {};
        if (filter_dict != null) {list_post_data.filter_dict = filter_dict;}
        if (exclude_dict != null) {list_post_data.exclude_dict = exclude_dict;}
        if (order_by != null) {list_post_data.order_by = order_by};
        return this.request_post(list_without_pag_url, list_post_data);
    }

    /**
     *Save an object at PumpWood. Objects without pk will be added and with the pk will be modified.
     *The model class of the object is obtained by model_class attribute at obj_dict parameter.
     *@param obj_dict - Object data to be added or updated.
    **/
    save(obj_dict:IPumpWoodObj):Observable<IPumpWoodObj>{
        let save_url:string;

        save_url = obj_dict.model_class.toLowerCase() + "/save/";
        return this.request_post(save_url, obj_dict );
    }

    /**
     *Delete an object at PumpWood.
     *The model class of the object is obtained by model_class attribute at obj_dict parameter.
     *@param obj_dict - Object to be deleted.
    **/
    delete(obj_dict:IPumpWoodObj):Observable<any>{
        let delete_url:string;

        if (obj_dict.pk == null){
            throw new Error('An object without pk can not be deleted');
        }

        delete_url = obj_dict.model_class.toLowerCase() + "/retrieve/" + obj_dict.pk.toString() + "/";
        return this.request_delete( delete_url );
    }

    /**
    *List all actions avaiables at the model_class
    *@param model_class - Name of the model. Ex.: DescriptionAttribute
    **/
    list_actions(model_class:string):Observable<IActionDescription[]>{
        let list_actions_url:string;

        list_actions_url = model_class.toLowerCase() + "/actions/";
        return this.request_get( list_actions_url );
    }

    /**
    *List all actions avaiables for selected object at the model_class
    *@param objects - Selected objects to check avaiable actions.
    **/
    list_objects_actions(objects:IPumpWoodObj[]):Observable<IActionDescription[]>{
        let model_class:string | null = null
        let list_actions_url:string;

        if (objects.length == 0) return from([]);
        for (let obj of objects){
          if (model_class === null) model_class = obj.model_class;
          if (model_class != obj.model_class) {
            throw new Error('Objects with different model_class');
          }
        }

        list_actions_url = model_class.toLowerCase() + "/actions/";
        return this.request_post(list_actions_url, objects);
    }

    /**
    *Execute an action over an object or at the class using the parameters.
    *@param model_class - Name of the model. Ex.: DescriptionAttribute.
    *@param action - Action name. Ex.: create_brother.
    *@param pk - Pk number of the object over which action will be applied.
    *@param parameters - Parameters used in the action.
    **/
    execute_action(model_class:string, action:string, pk:number | null
      , parameters:object):Observable<IActionResult>{
        let execute_action_url:string;

        execute_action_url = model_class.toLowerCase() + "/actions/" +
          action + "/";
        if (pk != null){
            execute_action_url = execute_action_url + pk.toString() + "/"
        }
        return this.request_post(execute_action_url, parameters );
    }

    /**
    *Options to be used at the objects search at lists end-points
    *@param model_class - Name of the model. Ex.: DescriptionAttribute.
    **/
    search_options(model_class:string):Observable<ISearchOptions>{
        let search_options_url:string;

        search_options_url = model_class.toLowerCase() + "/options/";
        return this.request_get(search_options_url);
    }

    /**
    *Options to be used when saving objects.
    *@param parcial_obj_dict - Parcial object data used as template to return the possibility of other object fields.
    **/
    fill_options(parcial_obj_dict:IPumpWoodObj, field:string=null):Observable<any>{
        let fill_options_url:string;

        fill_options_url = parcial_obj_dict.model_class.toLowerCase() + "/options/";
        if (field  !== null) fill_options_url = fill_options_url + field
        return this.request_post(fill_options_url, parcial_obj_dict);
    }

    /**
    *Options to be used when saving objects.
    *@param parcial_obj_dict - List of parcial object data used as template to return the possibility of other object fields.
    **/
    fill_list_options(parcial_obj_dict:IPumpWoodObj[], field:string=null):Observable<any>{
        let fill_options_url:string;
        let distinct_model_class:Set<string>;
        let array_model_class:string[] = [];

        for (let obj of parcial_obj_dict){
          array_model_class.push(obj.model_class)
        }

        distinct_model_class = new Set(array_model_class);
        if (distinct_model_class.size != 1){
          throw new Error('Trying to get fill_options_list from heterogeneous model_class list');
        }

        fill_options_url = parcial_obj_dict[0].model_class.toLowerCase() + "/options/";
        if (field  !== null) fill_options_url = fill_options_url + field
        return this.request_post(fill_options_url, parcial_obj_dict);
    }


    /**
    *Pivot model's for data models like DatabaseVariable, CalendarDataBase, GeoDataBaseVariable, ...
    *@param model_class - Name of the model that will be retrieved. Ex.: DescriptionAttribute, ModelQueue.
    *@param filter_dict - Filter parameters. Used at Where SQL clasuses
    *@param exclude_dict - Filter parameters. Used at where SQL clasuses with not
    *@param order_by - Order the results by the columns. Using '-' at the start of the column name will make ordering descendent
    *@param columns - Columns to pivot over.
    *@param format - Format used to pass DataFrame to dict.
    *See https://pandas.pydata.org/pandas-docs/stable/generated/pandas.DataFrame.to_dict.html.
    **/
    pivot(model_class:string, columns:string[], format:string
      , filter_dict:object, exclude_dict:object, order_by:string[]):Observable<any>{
        let pivot_url:string;
        let post_data:object;

        pivot_url = model_class.toLowerCase() + "/options/";
        post_data = {
            'columns':columns
          , 'format':format
          , 'filter_dict': filter_dict
          , 'exclude_dict': exclude_dict
          , 'order_by': order_by
        }
        return this.request_post( pivot_url, post_data )
    }
}
