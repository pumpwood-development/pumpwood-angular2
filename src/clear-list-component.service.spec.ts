import { TestBed, inject } from '@angular/core/testing';

import { ClearListComponentService } from './clear-list-component.service';

describe('ClearListComponentService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ClearListComponentService]
    });
  });

  it('should be created', inject([ClearListComponentService], (service: ClearListComponentService) => {
    expect(service).toBeTruthy();
  }));
});
