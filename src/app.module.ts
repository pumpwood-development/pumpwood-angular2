import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http'
import { InViewportModule } from 'ng-in-viewport';
import { PumpwoodAuthService } from './pumpwood-auth.service';
import { PumpwoodRestService } from './pumpwood-rest.service';
import { PumpwoodListComponent } from './list-component/pumpwood-list.component';
import { PumpwoodRetrieveComponent } from './retrieve-component/pumpwood-retrieve.component';
import { mergeOrderedObjectLists } from './functions'

@NgModule({
  declarations: [
      PumpwoodAuthService,
    , PumpwoodRestService,
    , PumpwoodListComponent
    , PumpwoodRetrieveComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    InViewportModule.forRoot()
  ],
  providers: [],
  exports: [
    PumpwoodAuthService,
    PumpwoodRestService,
    PumpwoodListComponent,
    PumpwoodRetrieveComponent
  ]
})
export class PumpWoodAngularModule { }
